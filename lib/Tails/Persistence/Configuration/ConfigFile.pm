=head1 NAME

Tails::Persistence::Configuration::ConfigFile - read, parse and write live-persistence.conf

=cut

package Tails::Persistence::Configuration::ConfigFile;
use strictures 2;
use Moo;
use MooX::late;
use MooX::HandlesVia;

use autodie qw(:all);
use warnings FATAL => 'all';
use Carp;
use Tails::Persistence::Configuration::Line;
use Types::Path::Tiny qw{AbsPath};

use namespace::clean;


=head1 ATTRIBUTES

=cut

has 'config_file_path' => (
    isa      => AbsPath,
    is       => 'ro',
    coerce   => AbsPath->coercion,
    required => 1,
);

has 'lines' => (
    lazy_build    => 1,
    is            => 'rw',
    isa           => 'ArrayRef[Tails::Persistence::Configuration::Line]',
    handles_via   => [ 'Array' ],
    handles       => {
        all_lines => 'elements',
    },
);


=head1 CONSTRUCTORS

=cut

sub BUILD {
    my $self = shift;

    $self->config_file_path->touch;
}

sub _build_lines {
    my $self = shift;
    return [
        grep { defined $_ } map {
            Tails::Persistence::Configuration::Line->new_from_string($_)
        } $self->config_file_path->lines({chomp => 1})
    ];
}


=head1 METHODS

=cut

=head2 output

Returns the in-memory configuration, as a string in the live-persistence.conf format.

=cut
sub output {
    my $self = shift;
    my $out = "";
    foreach ($self->all_lines) {
        $out .= $_->stringify . "\n";
    }
    return $out;
}

=head2 save

Save the in-memory configuration to disk.
Throw exception on error.

=cut
sub save {
    my $self = shift;
    my $output = $self->output;
    my $fh = $self->config_file_path->openw();
    print $fh $output;
    $fh->sync;
    close $fh;
    $self->config_file_path->chmod(0600);
}

no Moo;
1;
