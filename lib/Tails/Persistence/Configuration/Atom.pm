=head1 NAME

Tails::Persistence::Configuration::Atom - a GUI-friendly configuration line

=cut

package Tails::Persistence::Configuration::Atom;
use strictures 2;
use Moo;
use MooX::late;

with 'Tails::Persistence::Role::ConfigurationLine';

use autodie qw(:all);
use warnings FATAL => 'all';

use List::MoreUtils qw{all pairwise};

use namespace::clean;


=head1 ATTRIBUTES

=cut

has 'enabled' => (
    required => 1,
    is       => 'rw',
    isa      => 'Bool',
);

has 'id' => (
    lazy_build => 1,
    is         => 'rw',
    isa        => 'Str',
);


=head1 CONSTRUCTORS

=cut

sub new_from_line {
    my $class = shift;
    my $line  = shift;
    my %args  = @_;

    Tails::Persistence::Configuration::Atom->new(
        destination => $line->destination,
        options     => $line->options,
        %args,
    );
}

sub _build_id {
    my $self = shift;
    'Custom';
}


=head1 METHODS

=cut

sub equals_atom {
    my $self       = shift;
    my $other_atom = shift;
    $self->destination eq $other_atom->destination
        and
    $self->options_are(@{$other_atom->options});
}

sub equals_line {
    my $self = shift;
    my $line = shift;
    $self->destination eq $line->destination
        and
    $self->options_are($line->all_options);
}

sub options_are {
    my $self = shift;

    my @expected = sort(@_);
    my @options  = sort($self->all_options);

    return unless @expected == @options;

    all { $_ } pairwise {
        defined($a) and defined($b) and $a eq $b
    } @expected, @options;
}

no Moo;
1;
