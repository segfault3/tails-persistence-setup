=head1 NAME

Tails::Persistence::Configuration - manage live-persistence.conf and presets

=cut

package Tails::Persistence::Configuration;
use Moo;
use MooX::late;
use MooX::HandlesVia;

use strictures 2;
use autodie qw(:all);
use Carp;
use Carp::Assert::More;

use Tails::Persistence::Configuration::Atom;
use Tails::Persistence::Configuration::ConfigFile;
use Tails::Persistence::Configuration::Presets;
use Types::Path::Tiny qw{AbsPath};

use List::MoreUtils qw{none};
use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");

use namespace::clean;


=head1 ATTRIBUTES

=cut

has 'config_file_path' => (
    required  => 1,
    isa       => AbsPath,
    is        => 'rw',
    coerce    => AbsPath->coercion,
);

has 'file' => (
    lazy_build => 1,
    is         => 'rw',
    isa        => 'Tails::Persistence::Configuration::ConfigFile',
);
has 'presets' => (
    lazy_build => 1,
    is         => 'rw',
    isa        => 'Tails::Persistence::Configuration::Presets',
);
has 'force_enable_presets' => (
    is      => 'ro',
    isa     => 'ArrayRef[Str]',
    default => sub { [] },
);
has 'atoms' => (
    lazy_build => 1,
    is            => 'rw',
    isa           => 'ArrayRef[Tails::Persistence::Configuration::Atom]',
    handles_via   => 'Array',
    handles       => {
        all_atoms => 'elements',
        push_atom => 'push',
    },
);


=head1 CONSTRUCTORS

=cut

sub _build_file {
    my $self = shift;
    my $file = Tails::Persistence::Configuration::ConfigFile->new(
        config_file_path => $self->config_file_path
    );
    return $file;
}

sub _build_presets {
    my $self = shift;
    Tails::Persistence::Configuration::Presets->new();
}

sub _build_atoms {
    my $self = shift;
    return $self->merge_file_with_presets();
}

=head1 METHODS

=cut

sub lines_not_in_presets {
    my $self = shift;
    grep {
        my $line = $_;
        ! grep { $_->equals_line($line) } $self->presets->atoms
    } $self->file->all_lines;
}

sub atoms_not_in_presets {
    my $self = shift;
    grep {
        my $atom = $_;
        none { $atom->equals_atom($_) } $self->presets->atoms
    } $self->all_atoms;
}

sub merge_file_with_presets {
    my $self = shift;
    $self->presets->set_state_from_lines($self->file->all_lines);
    $self->presets->set_state_from_overrides($self->force_enable_presets);

    [
        $self->presets->atoms,
        map {
            Tails::Persistence::Configuration::Atom->new_from_line(
                $_,
                enabled => 1
            );
        } $self->lines_not_in_presets,
    ];
}

sub all_enabled_atoms {
    my $self = shift;
    grep { $_->enabled } $self->all_atoms;
}

sub all_enabled_lines {
    my $self = shift;
    map {
        Tails::Persistence::Configuration::Line->new(
            destination => $_->destination,
            options     => $_->options,
        )
    } $self->all_enabled_atoms;
}

sub save {
    my $self = shift;
    $self->file->lines([ $self->all_enabled_lines ]);
    $self->file->save;
}

no Moo;
1;
