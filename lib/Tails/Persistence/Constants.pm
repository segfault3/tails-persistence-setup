package Tails::Persistence::Constants;
use strictures 2;
use Moo;
use MooX::late;
use Types::Path::Tiny qw{AbsPath};

use autodie qw(:all);

use namespace::clean;


=head1 Attributes

=cut
for (qw{partition_label partition_guid filesystem_type filesystem_label}) {
    has "persistence_$_" => (
        lazy_build => 1,
        is         => 'ro',
        isa        => 'Str',
    );
}

has "persistence_minimum_size"       => (
    lazy_build => 1,
    is         => 'ro',
    isa        => 'Int',
);

has 'persistence_filesystem_options' => (
    lazy_build => 1,
    is         => 'ro',
    isa        => 'HashRef[Str]',
);

has 'persistence_state_file'         => (
    isa        => AbsPath,
    is         => 'ro',
    lazy_build => 1,
    coerce     => AbsPath->coercion,
    documentation => q{File where tails-greeter writes persistence state.},
);


=head1 Constructors

=cut
sub _build_persistence_partition_label {
    my $self = shift;
    'TailsData'
}
sub _build_persistence_minimum_size {
    my $self = shift;
    64 * 2 ** 20
}
sub _build_persistence_filesystem_type {
    my $self = shift;
    'ext4'
}
sub _build_persistence_filesystem_label {
    my $self = shift;
    'TailsData'
}

sub _build_persistence_filesystem_options {
    my $self = shift;
    {
        label => $self->persistence_filesystem_label,
    };
}

sub _build_persistence_partition_guid  {
    my $self = shift;
    '8DA63339-0007-60C0-C436-083AC8230908' # Linux reserved
}

sub _build_persistence_state_file {
    my $self = shift;
    '/var/lib/live/config/tails.persistence'
}

no Moo;
1; # End of Tails::Persistence::Constants
